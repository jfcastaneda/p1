package model.vo;

public class InfoTaxi 
{
	
	private String compania;
	
	private double ganancia;
	
	private int servicios;
	
	private double distancia;
	
	private long tiempo;
	
	public InfoTaxi(String compania, double ganancia,
			int servicios, double distancia, long tiempo)
	{
		this.compania = compania;
		this.ganancia = ganancia;
		this.servicios = servicios;
		this.distancia = distancia;
		this.tiempo = tiempo;
	}
	
	public String compania()
	{
		return compania;
	}
	
	public double ganancia()
	{
		return ganancia;
	}
	
	public int servicios()
	{
		return servicios;
	}
	
	public double distancia()
	{
		return distancia;
	}
	
	public long tiempo()
	{
		return tiempo;
	}
}
