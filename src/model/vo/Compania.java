package model.vo;

import model.data_structures.List;

public class Compania
{
	private String nombre;
	
	private List<Servicio> serviciosPorTaxiId;
	
	private Taxi taxiMasRentable;
	
	public Compania(String nombre)
	{
		this.nombre = nombre;
		serviciosPorTaxiId = new List<Servicio>();
	}
	
	public String nombre()
	{
		return nombre;
	}
	
	public List<Servicio> serviciosPorTaxiId()
	{
		return serviciosPorTaxiId;
	}
	
	public Taxi taxiMasRentable()
	{
		return taxiMasRentable;
	}
	
	public void cambiarTaxiMasRentable(Taxi taxi)
	{
		this.taxiMasRentable = taxi;
	}
	
	public String toString()
	{
		return nombre;
	}
}
