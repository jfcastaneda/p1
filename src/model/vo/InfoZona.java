package model.vo;

public class InfoZona 
{		
	private int iniciaronCantidad;
	
	private double iniciaronGanancia;
	
	private int terminaronCantidad;
	
	private double terminaronGanancia;
	
	private int ambasCantidad;
	
	private double ambasGanancia;
	
	public InfoZona(int iniciaronCantidad, double iniciaronGanancia,
			int terminaronCantidad, double terminaronGanancia, int ambasCantidad,
			double ambasGanancia)
	{
		this.iniciaronCantidad = iniciaronCantidad;
		this.iniciaronGanancia = iniciaronGanancia;
		this.terminaronCantidad = terminaronCantidad;
		this.terminaronGanancia = terminaronGanancia;
		this.ambasCantidad = ambasCantidad;
		this.ambasGanancia = ambasGanancia;
	}
	
	public int iniciaronCantidad()
	{
		return iniciaronCantidad;
	}
	
	public double iniciaronGanancia()
	{
		return iniciaronGanancia;
	}
	
	public int terminaronCantidad()
	{
		return terminaronCantidad;
	}
	
	public double terminaronGanancia()
	{
		return terminaronGanancia;
	}
	
	public int ambasCantidad()
	{
		return ambasCantidad;
	}
	
	public double ambasGanancia()
	{
		return ambasGanancia;
	}
}
