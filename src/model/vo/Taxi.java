package model.vo;

import model.data_structures.List;

public class Taxi 
{
	private String id;
	
	private List<Servicio> serviciosPorFecha;
	
	private Compania compania;
	
	public Taxi(String id, Compania compania)
	{
		this.id = id;
		this.compania = compania;
		serviciosPorFecha = new List<Servicio>();
	}
	
	public String id()
	{
		return id;
	}
	
	public List<Servicio> serviciosPorFecha()
	{
		return serviciosPorFecha;
	}
	
	public Compania compania()
	{
		return compania;
	}
	
	public String toString()
	{
		return id;
	}
}
