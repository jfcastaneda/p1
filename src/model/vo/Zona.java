package model.vo;

import model.data_structures.List;

public class Zona 
{
	private String id;
	
	private List<Servicio> serviciosPorFecha;
	
	public Zona(String id)
	{
		this.id = id;
		serviciosPorFecha = new List<Servicio>();
	}
	
	public String id()
	{
		return id;
	}
	
	public List<Servicio> serviciosPorFecha()
	{
		return serviciosPorFecha;
	}
	
	public String toString()
	{
		return id;
	}
}
