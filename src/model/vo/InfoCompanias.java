package model.vo;

import model.data_structures.List;

public class InfoCompanias 
{
	private int companiasTaxi;
	
	private int taxisCompania;
	
	private List<Compania> companias;
	
	public InfoCompanias(int companiasTaxi, int taxisCompania, 
			List<Compania> companias)
	{
		this.companiasTaxi = companiasTaxi;
		this.taxisCompania = taxisCompania;
		this.companias = companias;
	}
	
	public int companiasTaxi()
	{
		return companiasTaxi;
	}
	
	public int taxisCompanias()
	{
		return taxisCompania;
	}
	
	public List<Compania> companias()
	{
		return companias;
	}
}
