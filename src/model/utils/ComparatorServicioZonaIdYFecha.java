package model.utils;

import java.util.Comparator;

import model.vo.Servicio;

public class ComparatorServicioZonaIdYFecha implements Comparator<Servicio>
{
	public int compare(Servicio s1, Servicio s2)
	{
		int a = Integer.parseInt(s1.salida().id()), b = Integer.parseInt(s2.salida().id());
		
		if(a < b)
		{
			// TODO: Capaz y falla esta porquer�a.
			return -1;
		}
		else if(a == b)
		{
			if(s1.inicio() < s2.inicio())
			{
				return -1;
			}
			else if(s1.inicio() == s2.inicio())
			{
				return 0;
			}
			else
			{
				return 1;
			}
		}
		else
		{
			return 1;
		}
	}
}
