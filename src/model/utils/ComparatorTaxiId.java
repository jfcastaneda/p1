package model.utils;

import java.util.Comparator;

import model.vo.Taxi;

public class ComparatorTaxiId implements Comparator<Taxi>
{
	public int compare(Taxi t1, Taxi t2)
	{
		return t1.id().compareTo(t2.id());
	}	
}
