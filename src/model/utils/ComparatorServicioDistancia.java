package model.utils;

import java.util.Comparator;

import model.vo.Servicio;

public class ComparatorServicioDistancia implements Comparator<Servicio>
{
	public int compare(Servicio s1, Servicio s2)
	{
		if(s1.distancia() > s2.distancia())
		{
			return 1;
		}
		else if(s1.distancia() == s2.distancia())
		{
			return 0;
		}
		else
		{
			return -1;
		}
	}
}
