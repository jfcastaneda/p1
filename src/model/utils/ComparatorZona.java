package model.utils;

import java.util.Comparator;

import model.vo.Zona;

public class ComparatorZona implements Comparator<Zona>
{
	public int compare(Zona z1, Zona z2)
	{ 
		int id1 = Integer.parseInt(z1.id()), id2 = Integer.parseInt(z2.id());
		if(id1 < id2)
		{
			return -1;
		}
		else if(id1 == id2)
		{
			return 0;
		}
		else
		{
			return 1;
		}
	}
}
